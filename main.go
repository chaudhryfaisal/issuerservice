//go:generate protoc -I/usr/local/include -I=walletpb -I$GOPATH/src  -I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis --grpc-gateway_out=logtostderr=true:walletpb walletpb/walletpb.proto

package main

import (
	"fmt"
	"net"
	"net/http"

	"github.com/golang/glog"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/spf13/viper"
	"golang.org/x/net/context"
	"google.golang.org/grpc"

	config "bitbucket.org/objectcomputing/issuerservice/config"
	issuer "bitbucket.org/objectcomputing/issuerservice/issuerServer"
	"bitbucket.org/objectcomputing/issuerservice/walletpb"
)

func runGrpcServer(port int) error {
	conn, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		return err
	}
	s := grpc.NewServer()
	walletpb.RegisterIssuerServer(s, issuer.NewIssuerServer())
	fmt.Printf("grpc on port: %d\n", port)
	go s.Serve(conn)
	return nil
}

func newGateway(ctx context.Context, grpcPort int) (http.Handler, error) {

	gwmux := runtime.NewServeMux()
	dopts := []grpc.DialOption{grpc.WithInsecure()}
	err := walletpb.RegisterIssuerHandlerFromEndpoint(ctx, gwmux, fmt.Sprintf("localhost:%d", grpcPort), dopts)
	return gwmux, err
}

func serveHealth(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(200)
}

// Run the GRPC server and gateway
func Run(conf *config.Configuration) error {

	issuer.IssuerID = conf.IssuerID
	for _, customer := range conf.Customers {
		issuer.Customers[customer.UID] = customer.Wallets
	}

	if err := runGrpcServer(conf.GRPCPort); err != nil {
		glog.Fatal(err)
	}

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := http.NewServeMux()
	gwmux, err := newGateway(ctx, conf.GRPCPort)

	if err != nil {
		return err
	}

	mux.Handle("/", gwmux)
	mux.HandleFunc("/health", serveHealth)

	fmt.Printf("http on port: %d\n", conf.HTTPPort)
	if err := http.ListenAndServe(fmt.Sprintf(":%d", conf.HTTPPort), mux); err != nil {
		glog.Fatal(err)
	}
	return nil
}

func main() {

	viper.SetConfigName("config")
	viper.AddConfigPath("/etc/monster_mesh/issuer_service")
	viper.AddConfigPath(".")
	viper.SetDefault("http_port", 8080)
	viper.SetDefault("grpc_port", 9090)

	defer glog.Flush()

	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println("Config not found...")
	}

	conf := new(config.Configuration)

	err = viper.Unmarshal(&conf)
	if err != nil {
		fmt.Println("Unable to unmarshal customers")
	}

	Run(conf)
}
